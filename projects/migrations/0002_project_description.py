# Generated by Django 4.2.2 on 2023-06-05 21:27

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):
    dependencies = [
        ("projects", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="project",
            name="description",
            field=models.TextField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
