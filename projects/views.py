from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


@login_required
def project_list(request):
    project_lists = Project.objects.filter(owner=request.user)
    context = {
        "all_project_lists": project_lists,
    }

    return render(request, "projects/list.html", context)


@login_required
def project_detail(request, id):
    project_details = get_object_or_404(Project, id=id)
    tasks = project_details.tasks.all()
    context = {
        "project_detail": project_details,
        "tasks": tasks,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
